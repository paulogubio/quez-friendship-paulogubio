class Player < ActiveRecord::Base
  	validates_presence_of :name
  	has_many :friendships
	has_many :friends, :through => :friendships
	has_many :i_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
	has_many :i_friends, :through => :i_friendships, :source => :player
end
