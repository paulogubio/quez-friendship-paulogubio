class PlayersController < ApplicationController
  before_action :set_player, only: [:show, :edit, :update, :destroy,:create_friendship]
  respond_to :html, :json

  def index
    @players = Player.all
  end

  def new
    @player = Player.new
  end

  def create
    @player = Player.new(player_params)

    if @player.save
      redirect_to @player, notice: 'Player was successfully created.'
    else
      render :new
    end
  end

  def update
    if @player.update(player_params)
      redirect_to @player, notice: 'Player was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @player.destroy
    redirect_to players_url, notice: 'Player was successfully destroyed.'
  end

  def create_friendship
      @friendship = Friendship.new
      @friendship.player_id = session[:current_player_id]
      @friendship.friend_id = @player.id
      @friendship.save
      redirect_to players_url, notice: 'Friendship was successfully created.'
  end

  def destroy_friendship
    @friendship = Friendship.find(params[:id])
    @friendship.destroy    
    redirect_to players_url, notice: 'Friendship was successfully destroyed.'
  end
  
  private
  def set_player
    @player = Player.find(params[:id])
  end

  def player_params
    params.require(:player).permit(:name)
  end
end
