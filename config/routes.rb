Rails.application.routes.draw do
  get 'dashboard', to: "dashboard#index"

  root to: "players#index"

  resources :friendships

  resources :players do
    resources :player_sessions, only: [ :create ], path: "/sessions"
  end

  get '/players/create_friendship/:id' => 'players#create_friendship'
  get '/players/destroy_friendship/:id' => 'players#destroy_friendship'


end
